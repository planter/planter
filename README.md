# Planter

Planter is a shell and GUI application that creates empty folders.

No, really. That's what it does.

## Why would I use Planter?

You have to create folders for web sites, or for your code projects,
or for audio projects, or video projects. After the 900th time
creating the same 8 subdirectories, you might start looking for a way
to automate this process.

That's what Planter does for you.

## Usage

Planter is pretty simple to use.

1. Edit the default Planter config (''tree.list'') or create your own. The config file allows you to make new directories, copy files, and create symlinks.

2. Run Planter, giving it the type of project directory tree you want to create.

3. That's it.


## Why wouldn't you just automate this with BASH or Git or Make?

You are welcome to not use Planter and use your own automation
tool. Planter is just one of many options.


## Requirements

These are required:

* Python 3
* PyYaml

Planter can be used from the shell. If you want a GUI, install these
packages, as well:

* PyQt5
* python3-dbus